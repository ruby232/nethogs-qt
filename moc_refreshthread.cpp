/****************************************************************************
** Meta object code from reading C++ file 'refreshthread.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "refreshthread.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'refreshthread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_RefreshThread_t {
    QByteArrayData data[18];
    char stringdata0[160];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RefreshThread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RefreshThread_t qt_meta_stringdata_RefreshThread = {
    {
QT_MOC_LITERAL(0, 0, 13), // "RefreshThread"
QT_MOC_LITERAL(1, 14, 12), // "procDetected"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 8), // "procname"
QT_MOC_LITERAL(4, 37, 3), // "rcv"
QT_MOC_LITERAL(5, 41, 4), // "sent"
QT_MOC_LITERAL(6, 46, 3), // "pid"
QT_MOC_LITERAL(7, 50, 3), // "uid"
QT_MOC_LITERAL(8, 54, 7), // "devname"
QT_MOC_LITERAL(9, 62, 11), // "procDeleted"
QT_MOC_LITERAL(10, 74, 16), // "refresh_finished"
QT_MOC_LITERAL(11, 91, 11), // "cnxDetected"
QT_MOC_LITERAL(12, 103, 12), // "QHostAddress"
QT_MOC_LITERAL(13, 116, 8), // "sourceip"
QT_MOC_LITERAL(14, 125, 5), // "sport"
QT_MOC_LITERAL(15, 131, 6), // "destip"
QT_MOC_LITERAL(16, 138, 5), // "dport"
QT_MOC_LITERAL(17, 144, 15) // "update_cnx_list"

    },
    "RefreshThread\0procDetected\0\0procname\0"
    "rcv\0sent\0pid\0uid\0devname\0procDeleted\0"
    "refresh_finished\0cnxDetected\0QHostAddress\0"
    "sourceip\0sport\0destip\0dport\0update_cnx_list"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RefreshThread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,   39,    2, 0x06 /* Public */,
       9,    1,   52,    2, 0x06 /* Public */,
      10,    0,   55,    2, 0x06 /* Public */,
      11,    4,   56,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    1,   65,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::ULongLong, QMetaType::ULongLong, QMetaType::Int, QMetaType::UInt, QMetaType::QString,    3,    4,    5,    6,    7,    8,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 12, QMetaType::UShort, 0x80000000 | 12, QMetaType::UShort,   13,   14,   15,   16,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    6,

       0        // eod
};

void RefreshThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        RefreshThread *_t = static_cast<RefreshThread *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->procDetected((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint64(*)>(_a[2])),(*reinterpret_cast< quint64(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< uint(*)>(_a[5])),(*reinterpret_cast< const QString(*)>(_a[6]))); break;
        case 1: _t->procDeleted((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->refresh_finished(); break;
        case 3: _t->cnxDetected((*reinterpret_cast< const QHostAddress(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< const QHostAddress(*)>(_a[3])),(*reinterpret_cast< quint16(*)>(_a[4]))); break;
        case 4: _t->update_cnx_list((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (RefreshThread::*_t)(const QString & , quint64 , quint64 , int , unsigned int , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&RefreshThread::procDetected)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (RefreshThread::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&RefreshThread::procDeleted)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (RefreshThread::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&RefreshThread::refresh_finished)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (RefreshThread::*_t)(const QHostAddress & , quint16 , const QHostAddress & , quint16 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&RefreshThread::cnxDetected)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject RefreshThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_RefreshThread.data,
      qt_meta_data_RefreshThread,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *RefreshThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RefreshThread::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_RefreshThread.stringdata0))
        return static_cast<void*>(this);
    return QThread::qt_metacast(_clname);
}

int RefreshThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void RefreshThread::procDetected(const QString & _t1, quint64 _t2, quint64 _t3, int _t4, unsigned int _t5, const QString & _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void RefreshThread::procDeleted(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void RefreshThread::refresh_finished()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void RefreshThread::cnxDetected(const QHostAddress & _t1, quint16 _t2, const QHostAddress & _t3, quint16 _t4)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
