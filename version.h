#ifndef VERSION_H
#define VERSION_H

#define VERSION "0.6"

/*
 * 0.6 07/02/2020 Can chart a custom process
 * 0.5 12/04/2019 Add Statusbar to display status, Pause, horizontal Zoom when paused
 * 0.4 12/25/2016 small fixes
 * 0.3 12/22/2016 with cnx and nslookup.
 * 0.2 12/12/2016 with Qtcharts (kb/s is ok but not nice).
 * 0.1 11/29/2016 First version. kb/s is buggy.
 */

#endif // VERSION_H

