<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="20"/>
        <source>nethogs-qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="49"/>
        <source>PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="54"/>
        <source>User</source>
        <translation type="unfinished">Utilisateur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="59"/>
        <source>App</source>
        <translation type="unfinished">Application</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="64"/>
        <source>Status</source>
        <translation type="unfinished">Statut</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="69"/>
        <source>recv</source>
        <translation type="unfinished">reçu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="74"/>
        <source>sent</source>
        <translation type="unfinished">envoyé</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <source>recv/s</source>
        <translation type="unfinished">reçu/s</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <source>sent/s</source>
        <translation type="unfinished">envoyé/s</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <source>devname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>More</source>
        <translation type="unfinished">Plus</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="131"/>
        <source>Source IP</source>
        <translation type="unfinished">IP source</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <source>Source Port</source>
        <translation type="unfinished">Port source</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>Dest IP</source>
        <translation type="unfinished">IP Destination</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="146"/>
        <source>Dest Port</source>
        <translation type="unfinished">Port Destination</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>lookup</source>
        <translation type="unfinished">résoudre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="171"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="177"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="184"/>
        <source>View</source>
        <translation type="unfinished">&amp;Vue</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <source>&amp;About</source>
        <translation type="unfinished">&amp;A propos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="211"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished">Apropos de Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="234"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <source>Toggle global chart view</source>
        <translation type="unfinished">Basculer la vue du graphique global</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <source>Process chart view</source>
        <translation type="unfinished">Graphe d&apos;un process</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="96"/>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Global Chart</source>
        <translation type="unfinished">Vue globale</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="381"/>
        <source>About nethogs-qt</source>
        <translation type="unfinished">A propos de nethogs-qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="381"/>
        <source>nethogs-qt version %1
is a free software under GPL v3 !</source>
        <translation type="unfinished">nethogs-qt version %1
est un logiciel libre sous licence GPL v3 !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="463"/>
        <source>Running!</source>
        <translation type="unfinished">En fonctionement !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="466"/>
        <source>Paused!</source>
        <translation type="unfinished">En pause !</translation>
    </message>
</context>
<context>
    <name>SmallChart</name>
    <message>
        <location filename="smallchart.cpp" line="29"/>
        <source>RECV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="smallchart.cpp" line="30"/>
        <source>SENT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>process_chart_dialog</name>
    <message>
        <location filename="process_chart_dialog.ui" line="14"/>
        <source>Process chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="process_chart_dialog.ui" line="22"/>
        <source>Process name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="process_chart_dialog.ui" line="32"/>
        <source>Enable chart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
