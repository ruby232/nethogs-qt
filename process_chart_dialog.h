#ifndef PROCESS_CHART_DIALOG_H
#define PROCESS_CHART_DIALOG_H

#include <QDialog>

namespace Ui {
class process_chart_dialog;
}

class process_chart_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit process_chart_dialog(QWidget *parent = 0);
    ~process_chart_dialog();
    QString get_name();

private slots:
    void on_checkBox_chart_clicked();

private:
    Ui::process_chart_dialog *ui;
};

#endif // PROCESS_CHART_DIALOG_H
