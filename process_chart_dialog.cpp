/*
 * process_chart_dialog.cpp
 *
 * Copyright (c) 2020 Stéphane List
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "process_chart_dialog.h"
#include "ui_process_chart_dialog.h"

#include <QSettings>

process_chart_dialog::process_chart_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::process_chart_dialog)
{
    ui->setupUi(this);

    QSettings settings;
    QString pname = settings.value("process","").toString();
    ui->lineEdit_process_name->setText(pname);
}

process_chart_dialog::~process_chart_dialog()
{
    delete ui;
}

QString process_chart_dialog::get_name()
{
    return ui->lineEdit_process_name->text();
}

void process_chart_dialog::on_checkBox_chart_clicked()
{
    if (ui->checkBox_chart->isChecked())
    {
        ui->lineEdit_process_name->setEnabled(true);
    } else {
        ui->lineEdit_process_name->clear();
        ui->lineEdit_process_name->setEnabled(false);
    }
}
