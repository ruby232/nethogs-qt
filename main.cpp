/*
 * main.cpp
 *
 * Copyright (c) 2016 Stephane List
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QApplication::setOrganizationName("nethogs-qt");
    QApplication::setApplicationName("nethogs-qt");

    QTranslator qtTranslator;
    if (qtTranslator.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath)))
        app.installTranslator(&qtTranslator);
    else
        qDebug("Failed to load a translation for QT in your local language");

    QTranslator myTranslator;
    if (myTranslator.load("nethogs-qt_" + QLocale::system().name(), QCoreApplication::applicationDirPath()))
        app.installTranslator(&myTranslator);
    else
        qDebug() << "Failed to load a translation for the language:" << QLocale::system().name();

    // the translator must be created before the application's widgets.

    MainWindow w;
    w.show();

    return app.exec();
}
