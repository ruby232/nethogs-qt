/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[32];
    char stringdata0[456];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 15), // "new_total_speed"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 17), // "new_process_speed"
QT_MOC_LITERAL(4, 46, 11), // "reset_graph"
QT_MOC_LITERAL(5, 58, 11), // "refreshLine"
QT_MOC_LITERAL(6, 70, 8), // "procname"
QT_MOC_LITERAL(7, 79, 3), // "rcv"
QT_MOC_LITERAL(8, 83, 4), // "sent"
QT_MOC_LITERAL(9, 88, 3), // "pid"
QT_MOC_LITERAL(10, 92, 3), // "uid"
QT_MOC_LITERAL(11, 96, 7), // "devname"
QT_MOC_LITERAL(12, 104, 10), // "deleteLine"
QT_MOC_LITERAL(13, 115, 11), // "updateGraph"
QT_MOC_LITERAL(14, 127, 15), // "update_cnx_list"
QT_MOC_LITERAL(15, 143, 11), // "cnxDetected"
QT_MOC_LITERAL(16, 155, 12), // "QHostAddress"
QT_MOC_LITERAL(17, 168, 8), // "sourceip"
QT_MOC_LITERAL(18, 177, 5), // "sport"
QT_MOC_LITERAL(19, 183, 6), // "destip"
QT_MOC_LITERAL(20, 190, 5), // "dport"
QT_MOC_LITERAL(21, 196, 8), // "lookedUp"
QT_MOC_LITERAL(22, 205, 9), // "QHostInfo"
QT_MOC_LITERAL(23, 215, 4), // "host"
QT_MOC_LITERAL(24, 220, 27), // "on_actionAbout_Qt_triggered"
QT_MOC_LITERAL(25, 248, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(26, 273, 23), // "on_actionExit_triggered"
QT_MOC_LITERAL(27, 297, 24), // "on_actionReset_triggered"
QT_MOC_LITERAL(28, 322, 26), // "on_pushButton_back_clicked"
QT_MOC_LITERAL(29, 349, 24), // "on_actionPause_triggered"
QT_MOC_LITERAL(30, 374, 43), // "on_actionToggle_global_chart_..."
QT_MOC_LITERAL(31, 418, 37) // "on_actionProcess_chart_view_t..."

    },
    "MainWindow\0new_total_speed\0\0"
    "new_process_speed\0reset_graph\0refreshLine\0"
    "procname\0rcv\0sent\0pid\0uid\0devname\0"
    "deleteLine\0updateGraph\0update_cnx_list\0"
    "cnxDetected\0QHostAddress\0sourceip\0"
    "sport\0destip\0dport\0lookedUp\0QHostInfo\0"
    "host\0on_actionAbout_Qt_triggered\0"
    "on_actionAbout_triggered\0"
    "on_actionExit_triggered\0"
    "on_actionReset_triggered\0"
    "on_pushButton_back_clicked\0"
    "on_actionPause_triggered\0"
    "on_actionToggle_global_chart_view_triggered\0"
    "on_actionProcess_chart_view_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   99,    2, 0x06 /* Public */,
       3,    2,  104,    2, 0x06 /* Public */,
       4,    0,  109,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    6,  110,    2, 0x0a /* Public */,
      12,    1,  123,    2, 0x0a /* Public */,
      13,    0,  126,    2, 0x0a /* Public */,
      14,    1,  127,    2, 0x0a /* Public */,
      15,    4,  130,    2, 0x0a /* Public */,
      21,    1,  139,    2, 0x0a /* Public */,
      24,    0,  142,    2, 0x08 /* Private */,
      25,    0,  143,    2, 0x08 /* Private */,
      26,    0,  144,    2, 0x08 /* Private */,
      27,    0,  145,    2, 0x08 /* Private */,
      28,    0,  146,    2, 0x08 /* Private */,
      29,    0,  147,    2, 0x08 /* Private */,
      30,    0,  148,    2, 0x08 /* Private */,
      31,    0,  149,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::ULongLong, QMetaType::ULongLong,    2,    2,
    QMetaType::Void, QMetaType::ULongLong, QMetaType::ULongLong,    2,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::ULongLong, QMetaType::ULongLong, QMetaType::Int, QMetaType::UInt, QMetaType::QString,    6,    7,    8,    9,   10,   11,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 16, QMetaType::UShort, 0x80000000 | 16, QMetaType::UShort,   17,   18,   19,   20,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->new_total_speed((*reinterpret_cast< quint64(*)>(_a[1])),(*reinterpret_cast< quint64(*)>(_a[2]))); break;
        case 1: _t->new_process_speed((*reinterpret_cast< quint64(*)>(_a[1])),(*reinterpret_cast< quint64(*)>(_a[2]))); break;
        case 2: _t->reset_graph(); break;
        case 3: _t->refreshLine((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint64(*)>(_a[2])),(*reinterpret_cast< quint64(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< uint(*)>(_a[5])),(*reinterpret_cast< const QString(*)>(_a[6]))); break;
        case 4: _t->deleteLine((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->updateGraph(); break;
        case 6: _t->update_cnx_list((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->cnxDetected((*reinterpret_cast< const QHostAddress(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< const QHostAddress(*)>(_a[3])),(*reinterpret_cast< quint16(*)>(_a[4]))); break;
        case 8: _t->lookedUp((*reinterpret_cast< const QHostInfo(*)>(_a[1]))); break;
        case 9: _t->on_actionAbout_Qt_triggered(); break;
        case 10: _t->on_actionAbout_triggered(); break;
        case 11: _t->on_actionExit_triggered(); break;
        case 12: _t->on_actionReset_triggered(); break;
        case 13: _t->on_pushButton_back_clicked(); break;
        case 14: _t->on_actionPause_triggered(); break;
        case 15: _t->on_actionToggle_global_chart_view_triggered(); break;
        case 16: _t->on_actionProcess_chart_view_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QHostInfo >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (MainWindow::*_t)(quint64 , quint64 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::new_total_speed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(quint64 , quint64 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::new_process_speed)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::reset_graph)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::new_total_speed(quint64 _t1, quint64 _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::new_process_speed(quint64 _t1, quint64 _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::reset_graph()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
