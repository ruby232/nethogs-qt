/********************************************************************************
** Form generated from reading UI file 'process_chart_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROCESS_CHART_DIALOG_H
#define UI_PROCESS_CHART_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_process_chart_dialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *lineEdit_process_name;
    QCheckBox *checkBox_chart;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *process_chart_dialog)
    {
        if (process_chart_dialog->objectName().isEmpty())
            process_chart_dialog->setObjectName(QStringLiteral("process_chart_dialog"));
        process_chart_dialog->resize(807, 105);
        verticalLayout = new QVBoxLayout(process_chart_dialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(process_chart_dialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        lineEdit_process_name = new QLineEdit(process_chart_dialog);
        lineEdit_process_name->setObjectName(QStringLiteral("lineEdit_process_name"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit_process_name);

        checkBox_chart = new QCheckBox(process_chart_dialog);
        checkBox_chart->setObjectName(QStringLiteral("checkBox_chart"));
        checkBox_chart->setChecked(true);

        formLayout->setWidget(1, QFormLayout::FieldRole, checkBox_chart);


        verticalLayout->addLayout(formLayout);

        buttonBox = new QDialogButtonBox(process_chart_dialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(process_chart_dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), process_chart_dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), process_chart_dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(process_chart_dialog);
    } // setupUi

    void retranslateUi(QDialog *process_chart_dialog)
    {
        process_chart_dialog->setWindowTitle(QApplication::translate("process_chart_dialog", "Process chart", Q_NULLPTR));
        label->setText(QApplication::translate("process_chart_dialog", "Process name:", Q_NULLPTR));
        checkBox_chart->setText(QApplication::translate("process_chart_dialog", "Enable chart", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class process_chart_dialog: public Ui_process_chart_dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROCESS_CHART_DIALOG_H
